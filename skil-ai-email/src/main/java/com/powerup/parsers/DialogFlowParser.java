package com.powerup.parsers;

import com.google.cloud.dialogflow.v2.*;
import com.google.protobuf.Value;
import com.powerup.dto.DialogFlowOutputBean;

import java.util.*;
import java.util.logging.Logger;

public class DialogFlowParser {
  public static DialogFlowOutputBean detectIntentTexts(String projectId, List<String> texts, String sessionId,
                                                       String languageCode) throws Exception {
    // Instantiates a client
    try (SessionsClient sessionsClient = SessionsClient.create()) {
      // Set the session name using the sessionId (UUID) and projectID (my-project-id)
      SessionName session = SessionName.of(projectId, sessionId);
      System.out.println("Session Path: " + session.toString());

      // Detect intents for each text input
      for (String text : texts) {
        // Set the text (hello) and language code (en-US) for the query
        TextInput.Builder textInput = TextInput.newBuilder().setText(text).setLanguageCode(languageCode);

        // Build the query with the TextInput
        QueryInput queryInput = QueryInput.newBuilder().setText(textInput).build();

        // Performs the detect intent request
        DetectIntentResponse response = sessionsClient.detectIntent(session, queryInput);

        // Display the query result
        QueryResult queryResult = response.getQueryResult();
        Map<String, Value> fieldsMap = queryResult.getParameters().getFieldsMap();
        DialogFlowOutputBean dialogFlowOutputBean = new DialogFlowOutputBean();
        dialogFlowOutputBean.setIntent(queryResult.getIntent().getName());

        for (Map.Entry<String, Value> entry : fieldsMap.entrySet())
          if ("Technologies".equalsIgnoreCase(entry.getKey())) {
            LinkedList<String> technologiesReturnList = new LinkedList<>();
            List<Value> technologiesListValues = entry.getValue().getListValue().getValuesList();
            for(Value val: technologiesListValues) {
              technologiesReturnList.add(val.getStringValue());
            }
            dialogFlowOutputBean.setTechnologies(technologiesReturnList);
            if (!technologiesListValues.isEmpty())
              System.out.println("DialogFlowOutputBean Technologies -->" + dialogFlowOutputBean.getTechnologies());

          } else if ("MeetingTime".equalsIgnoreCase(entry.getKey())) {
            String meetingTime = entry.getValue().getStringValue();
            dialogFlowOutputBean.setMeetingTime(String.valueOf(meetingTime));
            if (meetingTime != "")
              System.out.println("DialogFlowOutputBean meetingTime -->" + dialogFlowOutputBean.getMeetingTime());

          } else if ("MeetingDate".equalsIgnoreCase(entry.getKey())) {
            String meetingDate = entry.getValue().getStringValue();
            dialogFlowOutputBean.setMeetingDate(String.valueOf(meetingDate));
            if (meetingDate != "")
              System.out.println("DialogFlowOutputBean meetingDate -->" + dialogFlowOutputBean.getMeetingDate());

          } else if ("MeetingType".equalsIgnoreCase(entry.getKey())) {
            String meetingType = entry.getValue().getStringValue();
            dialogFlowOutputBean.setMeetingType(String.valueOf(meetingType));
            if (meetingType != "")
              System.out.println("DialogFlowOutputBean meetingType -->" + dialogFlowOutputBean.getMeetingType());
          }
          else if ("Name".equalsIgnoreCase(entry.getKey())) {
            String name = entry.getValue().getStringValue();
            dialogFlowOutputBean.setName(String.valueOf(name));
            if (name != "")
              System.out.println("DialogFlowOutputBean name -->" + dialogFlowOutputBean.getName());
          }
          else if ("JobRoles".equalsIgnoreCase(entry.getKey())) {
            LinkedList<String> jobRolesReturnList = new LinkedList<>();
            List<Value> jobRolesListValues = entry.getValue().getListValue().getValuesList();
            for (Value val : jobRolesListValues) {
              jobRolesReturnList.add(val.getStringValue());
            }
            dialogFlowOutputBean.setJobRoles(jobRolesReturnList);
            if (!jobRolesListValues.isEmpty())
              System.out.println("DialogFlowOutputBean JobRoles -->" + dialogFlowOutputBean.getJobRoles());

          } else if ("SoftSkills".equalsIgnoreCase(entry.getKey())) {
            LinkedList<String> softskillsReturnList = new LinkedList<>();
            List<Value> softskillsListValues = entry.getValue().getListValue().getValuesList();
            for (Value val : softskillsListValues) {
              softskillsReturnList.add(val.getStringValue());
            }
            dialogFlowOutputBean.setSoftSkills(softskillsReturnList);
            if (!softskillsListValues.isEmpty())
              System.out.println("DialogFlowOutputBean softSkills -->" + dialogFlowOutputBean.getSoftSkills());
          } else if ("education".equalsIgnoreCase(entry.getKey())) {
            LinkedList<String> educationReturnList = new LinkedList<>();
            List<Value> educationListValues = entry.getValue().getListValue().getValuesList();
            for (Value val : educationListValues) {
              educationReturnList.add(val.getStringValue());
            }
            dialogFlowOutputBean.setEducations(educationReturnList);
            if (!educationListValues.isEmpty())
              System.out.println("DialogFlowOutputBean education -->" + dialogFlowOutputBean.getEducations());
          }

        System.out.println("====================");
        System.out.format("Query Text: '%s'\n", queryResult.getQueryText());
        System.out.format("Detected Intent: %s (confidence: %f)\n",
                queryResult.getIntent().getDisplayName(), queryResult.getIntentDetectionConfidence());
        System.out.format("Fulfillment Text: '%s'\n", queryResult.getFulfillmentText());
        return dialogFlowOutputBean;

      }
    }
    return null;
  }
  // [END dialogflow_detect_intent_text]
  public static DialogFlowOutputBean extractIntentAndEntities(String emailBodyText) throws Exception {
    ArrayList<String> texts = new ArrayList<>();
    String projectId = "skilappfinal";      //"powerup-dev-eb51e";
    String sessionId = UUID.randomUUID().toString();
    String languageCode = "en-US";
    texts.add(emailBodyText);
    return detectIntentTexts(projectId, texts, sessionId, languageCode);
  }
}
