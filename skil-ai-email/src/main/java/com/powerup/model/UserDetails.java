package com.powerup.model;

import org.bson.Document;
import java.io.Serializable;
import java.util.Date;

public class UserDetails extends Document implements Serializable {

  private String email;
  private String name;
  private String id;
  private String idToken;
  private String image;
  private String provider;
  private String token;
  private String createdAt;
  private String updatedAt;

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String created_at) {
    this.createdAt = createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

   public String getEmail() {    return email;  }

  public void setEmail(String email) {    this.email = email;  }

  public String getName() {    return name;  }

  public void setName(String name) {    this.name = name;  }

  public String getId() {    return id;  }

  public void setId(String id) {    this.id = id;  }

  public String getIdToken() {    return idToken;  }

  public void setIdToken(String idToken) {    this.idToken = idToken;  }

  public String getImage() {    return image;  }

  public void setImage(String image) {    this.image = image;  }

  public String getProvider() {    return provider;  }

  public void setProvider(String provider) {    this.provider = provider;  }

  public String getToken() {    return token;  }

  public void setToken(String token) {    this.token = token;  }

  @Override
  public String toString() {
    return "UserEmail{" +
            "email='" + email + '\'' +
            ", name='" + name + '\'' +
            ", id='" + id + '\'' +
            ", idToken='" + idToken + '\'' +
            ", image='" + image + '\'' +
            ", provider='" + provider + '\'' +
            ", token='" + token + '\'' +
            '}';
  }
}
