package com.powerup.email;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.powerup.email.template.VelocityEmailTemplate;
import com.powerup.model.UserDetails;
import com.powerup.parsers.DialogFlowParser;
import com.powerup.dto.DialogFlowOutputBean;
import hello.Greeting;
import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.message.DefaultBodyDescriptorBuilder;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.BodyDescriptorBuilder;
import org.apache.james.mime4j.stream.MimeConfig;
import org.bson.Document;
import org.bson.codecs.configuration.CodecConfigurationException;
import org.bson.conversions.Bson;
import org.jsoup.Jsoup;
import org.springframework.web.bind.annotation.*;
import tech.blueglacier.email.Attachment;
import tech.blueglacier.email.Email;
import tech.blueglacier.parser.CustomContentHandler;

import javax.mail.MessagingException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping(path = {"/v1/api"})

public class MailController {
  private AtomicLong counter = new AtomicLong();
  private Gson gson = new Gson();

  @GetMapping(path = ("/ping"))
  public String ping() {

    return "I'm running :--)))";
  }

  @PostMapping(path = ("/jsonPost"))
  public String processMail(@RequestBody String input) {
    System.out.println("calling request body -->" + input);
    return input;
  }

  @RequestMapping(path = "/processMail")
  public Greeting greeting(@PathVariable String bucket, @PathVariable String key) {
    Greeting retObj = new Greeting();
    retObj.setId(counter.incrementAndGet() + 9218);
    AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();

    try {
      S3Object o = s3.getObject(bucket, key);
      S3ObjectInputStream s3is = o.getObjectContent();
      processEmail(bucket, key, retObj, s3is);
    } catch (Exception e) {
      System.err.println("Exception occured while fetching email from S3 or while processing the email. Here are the detials: " + e.fillInStackTrace());
    }
    return retObj;
  }

  public void processEmail(String bucket, String key, Greeting retObj, InputStream is) throws MimeException, IOException, MessagingException, CodecConfigurationException {
    ContentHandler contentHandler = new CustomContentHandler();
    MimeConfig mime4jParserConfig = MimeConfig.DEFAULT;
    BodyDescriptorBuilder bodyDescriptorBuilder = new DefaultBodyDescriptorBuilder();
    MimeStreamParser mime4jParser = new MimeStreamParser(mime4jParserConfig, DecodeMonitor.SILENT, bodyDescriptorBuilder);
    mime4jParser.setContentDecoding(true);
    mime4jParser.setContentHandler(contentHandler);
    mime4jParser.parse(is);
    Email email = ((CustomContentHandler) contentHandler).getEmail();
    List<Attachment> attachments = email.getAttachments();
    Attachment calendar = email.getCalendarBody();
    Attachment htmlBody = email.getHTMLEmailBody();
    Attachment plainText = email.getPlainTextEmailBody();
    System.out.println("plaintext is " + plainText);
    String to = email.getToEmailHeaderValue();
    String cc = email.getCCEmailHeaderValue();
    String from = email.getFromEmailHeaderValue();
    StringWriter writer = new StringWriter();
    IOUtils.copy(email.getPlainTextEmailBody().getIs(), writer, "UTF-8");
    String emailBodyText = writer.toString();
    System.out.println("Plain email body string: " + emailBodyText);
    String emailPlainBodyText;
    StringBuilder retContent = new StringBuilder();
    retContent.append("bucket ->" + bucket).append("~~~key->" + key);
    retContent.append("~~~to->" + to).append("~~~from->" + from).append("~~~subject->" + email.getEmailSubject()).append("~~~plainText\n->" + emailBodyText)
            .append("headers -->" + gson.toJson(email.getHeader()));
    System.out.println("Content is ->" + retContent);
    MongoClientURI uri = new MongoClientURI(
            "mongodb://madmin:madmindev001@cluster0-shard-00-00-lwae7.mongodb.net:27017,cluster0-shard-00-01-lwae7.mongodb.net:27017,cluster0-shard-00-02-lwae7.mongodb.net:27017/powerup?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin");
    MongoClient mongoClient = new MongoClient(uri);
    MongoDatabase database = mongoClient.getDatabase("powerup");
    MongoCollection<Document> testCollection = database.getCollection("lucy_ai_emails");

    if (emailBodyText.contains("corelationId:")) {
      if (emailBodyText != null) {
        emailPlainBodyText = emailBodyText.replaceAll("[\\n\\r]", "").replaceAll("( )+", " ");
      } else {
        StringWriter writer2 = new StringWriter();
        IOUtils.copy(email.getHTMLEmailBody().getIs(), writer2, "UTF-8");
        String emailHtmlBodyString = Jsoup.parse(writer2.toString()).text();
        System.out.println("Plain html body string: " + emailHtmlBodyString);
        emailPlainBodyText = emailHtmlBodyString;
      }
      System.out.println("Email to ->" + to + " ~~~ CC ->" + cc);
      String strWithoutSpace = emailPlainBodyText.replaceAll("[\\n\\r]", "").replaceAll("( )+", " ");
      int s1 = strWithoutSpace.indexOf("xx1") + 3;
      int s2 = strWithoutSpace.indexOf("yy1");
      String uniqueId1 = strWithoutSpace.substring(s1, s2);
      String uniqueId2 = "<table style='opacity:0;'><tr><td>corelationId: xx1" + uniqueId1 + "yy1</td></tr></table>";
    //  String uniqueId2 = "<div class="mobile" style="mso-hide: all; overflow: hidden; max-height: 0; width: 0; display: none; line-height: 0; visibility: hidden;\">"
      System.out.println("EMAIL BODY TEXT:" + strWithoutSpace);
      Document doc = new Document();
      doc.append("from", from);
      doc.append("to", to);
      doc.append("subject", email.getEmailSubject());
      doc.append("message", emailPlainBodyText);
      doc.append("uniqueId", uniqueId1);
      testCollection.insertOne(doc);
      System.out.println("Created First Document Successfully" + doc);
      try {
        DialogFlowOutputBean parserQueryResult = DialogFlowParser.extractIntentAndEntities(strWithoutSpace);
        String to1 = from;
        String[] cc1 = AWSSESEmailSender.cc;
        String[] bcc1 = AWSSESEmailSender.bcc;
        if ((parserQueryResult.getMeetingTime() != "") && (parserQueryResult.getMeetingDate() != "")) {
          //String messageBody1 = VelocityEmailTemplate.meetingConfirmationTemplate(parserQueryResult).toString();
          //AWSSESEmailSender.messageBody = uniqueId2 + messageBody1;
          Icalendar.messageBody = uniqueId2 + Icalendar.messageBody;
          if (cc1 != null || bcc1 != null)
            Icalendar.sendMail(to1, "testMail", Icalendar.messageBody, cc1, bcc1, true);
          else
            Icalendar.sendMail(to1, "TestMail", AWSSESEmailSender.messageBody, true);

        } else {
          String messageBody1 = VelocityEmailTemplate.missingTimeDateTemplate(parserQueryResult).toString();
          AWSSESEmailSender.messageBody = uniqueId2 + messageBody1;

          if (cc1 != null || bcc1 != null)
            AWSSESEmailSender.sendMail(to1, "testMail", AWSSESEmailSender.messageBody, cc1, bcc1, false);
          else
            AWSSESEmailSender.sendMail(to1, "TestMail", AWSSESEmailSender.messageBody, false);
        }
        Document doc1 = new Document();
        doc1.append("from", to);
        doc1.append("to", from);
        doc1.append("cc", Arrays.asList(cc1));
        doc1.append("subject", email.getEmailSubject());
        doc1.append("message", AWSSESEmailSender.messageBody);
        doc1.append("uniqueId", uniqueId1);
        testCollection.insertOne(doc1);
        System.out.println("Created second Document Successfully" + doc1);
      } catch (CodecConfigurationException e) {
        System.err.println(e.getClass().getName() + ": " + e.getMessage());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }


    else {
      if (emailBodyText != null) {
        emailPlainBodyText = emailBodyText.replaceAll("[\\n\\r]", "").replaceAll("( )+", " ");
      } else {
        StringWriter writer2 = new StringWriter();
        IOUtils.copy(email.getHTMLEmailBody().getIs(), writer2, "UTF-8");
        String emailHtmlBodyString = Jsoup.parse(writer2.toString()).text();
        System.out.println("Plain html body string: " + emailHtmlBodyString);
        emailPlainBodyText = emailHtmlBodyString;
      }
      System.out.println("Email to ->" + to + " ~~~ CC ->" + cc);
      String strWithoutSpace = emailPlainBodyText.replaceAll("[\\n\\r]", "").replaceAll("( )+", " ");
      System.out.println("EMAIL BODY TEXT:" + strWithoutSpace);
      Document doc = new Document();
      doc.append("from", from);
      doc.append("to", to);
      doc.append("messageId", email.getHeader().getField("Message-ID"));
      doc.append("subject", email.getEmailSubject());
      doc.append("message", emailPlainBodyText);
      doc.append("uniqueId", AWSSESEmailSender.uniqueId);
      testCollection.insertOne(doc);
      System.out.println("Created First Document Successfully" + doc);
      try {
        DialogFlowOutputBean parserQueryResult = DialogFlowParser.extractIntentAndEntities(strWithoutSpace);
        String to1 = from;
        String[] cc1 = AWSSESEmailSender.cc;
        String[] bcc1 = AWSSESEmailSender.bcc;
        // if (!(parserQueryResult.getTechnologies().isEmpty()) && (parserQueryResult.getMeetingTime() != "") && (parserQueryResult.getMeetingDate() != "") && (parserQueryResult.getMeetingType() != "")) {
        if ((parserQueryResult.getMeetingTime() != "") && (parserQueryResult.getMeetingDate() != "")) {
          //String messageBody1 = VelocityEmailTemplate.meetingConfirmationTemplate(parserQueryResult).toString();
          //AWSSESEmailSender.messageBody = messageBody1;
          if (cc1 != null || bcc1 != null)
           Icalendar.sendMail(to1, "testMail", Icalendar.messageBody, cc1, bcc1, true);
          else
            Icalendar.sendMail(to1, "TestMail", AWSSESEmailSender.messageBody, true);

        } else {
          String messageBody1 = VelocityEmailTemplate.missingTimeDateTemplate(parserQueryResult).toString();
          AWSSESEmailSender.messageBody = messageBody1;
          if (cc1 != null || bcc1 != null)
            AWSSESEmailSender.sendMail(to1, "testMail", AWSSESEmailSender.messageBody, cc1, bcc1, true);
          else
            AWSSESEmailSender.sendMail(to1, "TestMail", AWSSESEmailSender.messageBody, true);
        }
        int s1 = AWSSESEmailSender.uniqueId.indexOf("xx1") + 3;
        int s2 = AWSSESEmailSender.uniqueId.indexOf("yy1");
        String uniqueId11 = AWSSESEmailSender.uniqueId.substring(s1, s2);
        AWSSESEmailSender.uniqueId = uniqueId11;
        Document doc1 = new Document();
        doc1.append("from", to);
        doc1.append("to", from);
        doc1.append("cc", Arrays.asList(cc1));
        doc1.append("subject", email.getEmailSubject());
        doc1.append("message", AWSSESEmailSender.messageBody);
        doc1.append("uniqueId", AWSSESEmailSender.uniqueId);
        testCollection.insertOne(doc1);
        System.out.println("Created second Document Successfully" + doc1);
      } catch (CodecConfigurationException e) {
        System.err.println(e.getClass().getName() + ": " + e.getMessage());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    MailController mailController = new MailController();
    String fileName = "E:/SkilappProject/powerup-ai-email/test_data/demoMail.eml";
    try (FileInputStream fis = new FileInputStream(new File(fileName))) {
      mailController.processEmail("TEST_BUCKET", "TEST_KEY", new Greeting(), fis);
     // mailController.storeUserDetails("TETSTS","TESTTTS","GHHfbds","fgbdjg","fgnfdg","dagfg","fgagfd","fgf");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (MimeException | MessagingException e) {
      e.printStackTrace();
    }
  }
}
