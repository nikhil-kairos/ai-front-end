package com.powerup.email;

import com.mongodb.*;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;


public class SampleRxTestDAO {
  public static void main(String args[]) {
    MongoClientURI uri = new MongoClientURI(
            "mongodb://madmin:madmindev001@cluster0-shard-00-00-lwae7.mongodb.net:27017,cluster0-shard-00-01-lwae7.mongodb.net:27017,cluster0-shard-00-02-lwae7.mongodb.net:27017/powerup?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin");

    MongoClient mongoClient = new MongoClient(uri);
    try {
      MongoDatabase database = mongoClient.getDatabase("powerup");
      MongoCollection<Document> collection = database.getCollection("lucy_ai_emails");
      /*Document doc = new Document();
      doc.append("from", AWSSESEmailSender.from);
      collection.insertOne(doc);
      System.out.println("insert document: " + doc);*/
    /*
      MongoCursor<Document> cursor = collection.find().iterator();   //for all documents
      while(cursor.hasNext()) {
        System.out.println(cursor.next());
      }
    */
   /* BasicDBObject basic1 = new BasicDBObject();
    basic1.put("from", AWSSESEmailSender.from);
    collection.insertOne(basic1);*/

      /*Document whereQuery = new Document();
      whereQuery.put("from", AWSSESEmailSender.from);
      MongoCursor<Document> cursor = collection.find(whereQuery).iterator();
      while(cursor.hasNext()) {
        System.out.println(cursor.next());
      }*/
      Document andQuery = new Document();
      List<Document> obj = new ArrayList<Document>();
      obj.add(new Document("from", AWSSESEmailSender.from));
      obj.add(new Document("UNIQUEID",AWSSESEmailSender.uniqueId));
      andQuery.put("$and", obj);

      System.out.println(andQuery.toString());

      MongoCursor<Document> cursor = collection.find(andQuery).iterator();
      while (cursor.hasNext()) {
        System.out.println(cursor.next());
      }

    } finally {
      //Close the client and release resources.
      mongoClient.close();
    }
    return ;
  }
  }


