package com.powerup.email;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.UUID;


public class Icalendar {

  public static String from = "veni827@gmail.com";
  public static String fromname = "Veni";
  public static String to = "hemareddy0419@gmail.com";
  public static String[] cc = {"ninjaslucy@gmail.com"};
  public static String[] bcc = {"veni.kairos@gmail.com"};
  public static String subject = "Amazon SES Test Mail";
  public static String messageBody = "" + "";
  public static String uniqueId = "";


  public static void commonMailMethod(String to, String subject, String messageBody, String[] cc, String[] bcc, boolean createUID) throws IOException, MessagingException {
    Properties props = System.getProperties();

    // Create a Session object to represent a mail session with the specified properties.
    Session session = Session.getDefaultInstance(props);
    MimeMessage msg = new MimeMessage(session);
    msg.setFrom(new InternetAddress(from, fromname));
    msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
    msg.setSubject(subject);
    if (!createUID) {
      System.out.println("***********************************");
    } else {
      String random_id = UUID.randomUUID().toString();
      uniqueId = "<table style='opacity:0;' ><tr><td>corelationId: xx1" + random_id + "yy1</td></tr></table>";
    }
    messageBody = uniqueId + messageBody;
    msg.setContent(messageBody, "text/html");
    StringBuffer sb = new StringBuffer();

    StringBuffer buffer = sb.append("BEGIN:VCALENDAR\n" +
            "PRODID:-//Google Inc//Google Calendar 70.9054//EN\n" +
            "VERSION:2.0\n" +
            "CALSCALE:GREGORIAN\n" +
            "METHOD:REQUEST\n" +
            "BEGIN:VEVENT\n" +
            "DTSTART:20181023T12500Z\n" +
            "DTEND:20181023T13100Z\n" +
            "DTSTAMP:20180918T185343Z\n" +
            "LOCATION:Conference room\n" +
            "DESCRIPTION:Hi Hema, Your Skil App [Screening] meeting has been confirmed successfully with :- Veni at Sep 26 2018 10:00 AM EDT\n"+
            "ORGANIZER;CN=\"Skil App Team\":mailto:veni.kairos@gmail.com\n" +
            "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE:MAILTO:hema.s@kairostech.com\n" +
//            "TRANSP:OPAQUE\n" +
//            "SEQUENCE:0\n" +
            "SUMMARY:Test meeting request\n" +
            "PRIORITY:5\n" +
            "CLASS:PUBLIC\n" +
            "BEGIN:VALARM\n" +
            "TRIGGER:PT10M\n" +
            "ACTION:EMAIL\n" +
            "DESCRIPTION:Skil:?? mentor meeting confirmation on:Systems En... at Sep 26\n" +
            " 2018 10:00 AM EDT\n" +
            "END:VALARM\n" +
            "BEGIN:VALARM\n" +
            "ACTION:AUDIO\n" +
            "TRIGGER:-PT10M\n" +
            "ATTACH;VALUE=URI:Basso\n" +
            "END:VALARM\n" +
            "CATEGORIES:Meeting\n" +
            "URL;VALUE=URI:https://www.skilapp.com/ \n" +
            "STATUS:TENTATIVE\n" +
            "END:VEVENT\n" +
            "END:VCALENDAR");
    BodyPart messageBodyPart = new MimeBodyPart();

    // Fill the message
    messageBodyPart.setHeader("Content-Class", "urn:content-  classes:calendarmessage");
    messageBodyPart.setHeader("Content-ID", "calendar_message");
    messageBodyPart.setDataHandler(new DataHandler(
            new ByteArrayDataSource(buffer.toString(), "text/calendar")));// very important

    // Create a Multipart
    Multipart multipart = new MimeMultipart();
    multipart.addBodyPart(messageBodyPart);

    // Put parts in message
    msg.setContent(multipart);
    InternetAddress[] ccAddress = new InternetAddress[cc.length];
    for (int i = 0; i < cc.length; i++) {
      ccAddress[i] = new InternetAddress(cc[i]);
    }

    for (int i = 0; i < ccAddress.length; i++) {
      msg.addRecipient(Message.RecipientType.CC, ccAddress[i]);
    }
    InternetAddress[] bccAddress = new InternetAddress[bcc.length];
    for (int i = 0; i < bcc.length; i++) {
      bccAddress[i] = new InternetAddress(bcc[i]);
    }
    for (int i = 0; i < bccAddress.length; i++) {
      msg.addRecipient(Message.RecipientType.BCC, bccAddress[i]);
    }
    // send the email.
    try {
      AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
      // Print the raw email content on the console
      PrintStream out = System.out;
      msg.writeTo(out);
      // Send the email.
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      msg.writeTo(outputStream);
      RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
      SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
      //.withConfigurationSetName(CONFIGURATION_SET);
      client.sendRawEmail(rawEmailRequest);
      System.out.println("Email sent!");
    } catch (Exception ex) {
      System.out.println("Email Failed");
      System.err.println("Error message: " + ex.getMessage());
      ex.printStackTrace();
    }
  }

  public static void sendMail(String to, String subject, String messageBody, String[] cc, String[] bcc, boolean createUID) throws Exception {
    commonMailMethod(to, subject, messageBody, cc, bcc, createUID);
  }

  public static void sendMail(String to, String subject, String messageBody, boolean createUID) throws Exception {
    commonMailMethod(to, subject, messageBody, new String[]{""}, new String[]{""}, createUID);
  }

}


