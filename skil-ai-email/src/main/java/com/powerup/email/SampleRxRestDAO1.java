package com.powerup.email;

import com.mongodb.*;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;


class SampleRxTestDAO1 {
  public static void main(String args[]) {
    MongoClientURI uri = new MongoClientURI(
            "mongodb://madmin:madmindev001@cluster0-shard-00-00-lwae7.mongodb.net:27017,cluster0-shard-00-01-lwae7.mongodb.net:27017,cluster0-shard-00-02-lwae7.mongodb.net:27017/powerup?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin");

    MongoClient mongoClient = new MongoClient(uri);
    try {
      MongoDatabase database = mongoClient.getDatabase("powerup");
      MongoCollection<Document> collection = database.getCollection("lucy_ai_emails");
      Document doc = new Document();
      doc.append("from", AWSSESEmailSender.from);
      collection.insertOne(doc);
      System.out.println("insert document: " + doc);
    } finally {
      //Close the client and release resources.
      mongoClient.close();
    }
    return ;
  }
}


