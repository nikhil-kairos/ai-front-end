package com.powerup.email;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.UUID;


public class AWSSESEmailSender {
  public static String from = "veni827@gmail.com";
  public static String fromname = "Veni";
  public static String to = "hemareddy0419@gmail.com";
  public static String[] cc = {"nikhidas@gmail.com"};
  public static String[] bcc = {"veni.kairos@gmail.com"};
  public static String subject = "Amazon SES Test Mail";
  public static String messageBody = "" + "";
  public static String uniqueId = "";


  public static void sendMail(String to, String subject, String messageBody, String[] cc, String[] bcc, boolean createUID) throws UnsupportedEncodingException, MessagingException {
    Properties props = System.getProperties();

    // Create a Session object to represent a mail session with the specified properties.
    Session session = Session.getDefaultInstance(props);
    MimeMessage msg = new MimeMessage(session);
    msg.setFrom(new InternetAddress(from, fromname));
    msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
    msg.setSubject(subject);
    if (!createUID) {
      System.out.println("***********************************");
    } else {
      String random_id = UUID.randomUUID().toString();
      uniqueId = "<table style='opacity:0;' ><tr><td>corelationId: xx1" + random_id + "yy1</td></tr></table>";
    }
    messageBody = uniqueId + messageBody;

    msg.setContent(messageBody, "text/html");
    InternetAddress[] ccAddress = new InternetAddress[cc.length];
    for (int i = 0; i < cc.length; i++) {
      ccAddress[i] = new InternetAddress(cc[i]);
    }

    for (int i = 0; i < ccAddress.length; i++) {
      msg.addRecipient(Message.RecipientType.CC, ccAddress[i]);
    }
    InternetAddress[] bccAddress = new InternetAddress[bcc.length];
    for (int i = 0; i < bcc.length; i++) {
      bccAddress[i] = new InternetAddress(bcc[i]);
    }
    for (int i = 0; i < bccAddress.length; i++) {
      msg.addRecipient(Message.RecipientType.BCC, bccAddress[i]);
    }
    // send the email.
    try {
      AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
      // Print the raw email content on the console
      PrintStream out = System.out;
      msg.writeTo(out);
      // Send the email.
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      msg.writeTo(outputStream);
      RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
      SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
      //.withConfigurationSetName(CONFIGURATION_SET);
      client.sendRawEmail(rawEmailRequest);
      System.out.println("Email sent!");
    } catch (Exception ex) {
      System.out.println("Email Failed");
      System.err.println("Error message: " + ex.getMessage());
      ex.printStackTrace();
    }
  }

  public static void sendMail(String to, String subject, String messageBody, boolean createUID) throws UnsupportedEncodingException, MessagingException {
    Properties props = System.getProperties();

    // Create a Session object to represent a mail session with the specified properties.
    Session session = Session.getDefaultInstance(props);
    MimeMessage msg = new MimeMessage(session);
    msg.setFrom(new InternetAddress(from, fromname));
    msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
    msg.setSubject(subject);
    if (!createUID) {
      System.out.println("***********************************");
    } else {
      String random_id = UUID.randomUUID().toString();
      uniqueId = "<table style='opacity:0;' ><tr><td>corelationId: xx1" + random_id + "yy1</td></tr></table>";
    }
    messageBody = uniqueId + messageBody;

    msg.setContent(messageBody, "text/html");

    // send the email.
    try {
      AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
      // Print the raw email content on the console
      PrintStream out = System.out;
      msg.writeTo(out);
      // Send the email.
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      msg.writeTo(outputStream);
      RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
      SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
      //.withConfigurationSetName(CONFIGURATION_SET);
      client.sendRawEmail(rawEmailRequest);
      System.out.println("Email sent!");
    } catch (Exception ex) {
      System.out.println("Email Failed");
      System.err.println("Error message: " + ex.getMessage());
      ex.printStackTrace();
    }
  }
}
