package com.powerup.email.template;

import com.powerup.dto.DialogFlowOutputBean;
import com.powerup.email.AWSSESEmailSender;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;
import java.util.UUID;


public class VelocityEmailTemplate {
  //public static void main(String[] args) {
  public static StringWriter meetingConfirmationTemplate(DialogFlowOutputBean dialogFlowOutputBean) {
    VelocityEngine velocity = new VelocityEngine();
    velocity.setProperty("resource.loader", "class");
    velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Template template = velocity.getTemplate("email-templates/meeting_confirmation_template.html");
    velocity.init();
    VelocityContext context = new VelocityContext();
    context.put("from", AWSSESEmailSender.from);
    context.put("fromname", AWSSESEmailSender.fromname);
    context.put("to", AWSSESEmailSender.to);
    context.put("bcc", AWSSESEmailSender.bcc[0]);
    context.put("title", "Apache Velocity");
    context.put("subject", AWSSESEmailSender.subject);
    context.put("messageBody", AWSSESEmailSender.messageBody);

    try {
      //DialogFlowOutputBean parserQueryResult = DialogFlowParser.extractIntentAndEntities();
      System.out.println("First Result" + dialogFlowOutputBean);
      context.put("Technologies", dialogFlowOutputBean.getTechnologies());
      context.put("meetingTime", dialogFlowOutputBean.getMeetingTime());
      context.put("meetingDate", dialogFlowOutputBean.getMeetingDate());
      context.put("meetingType", dialogFlowOutputBean.getMeetingType());
      context.put("name", dialogFlowOutputBean.getName());

      StringWriter writer = new StringWriter();
      template.merge(context, writer);
      return writer;
    } catch (Exception ex) {
      System.out.println("Error message: " + ex.getMessage());
    }
    return null;
  }

  public static StringWriter meetingMentorConfirmationTemplate(DialogFlowOutputBean dialogFlowOutputBean) {
    VelocityEngine velocity = new VelocityEngine();
    velocity.setProperty("resource.loader", "class");
    velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Template template = velocity.getTemplate("email-templates/meeting_mentor_confirmation_template.html");
    velocity.init();

    VelocityContext context = new VelocityContext();
    try {
      System.out.println("First Result" + dialogFlowOutputBean);
      context.put("Technologies", dialogFlowOutputBean.getTechnologies());
      context.put("meetingTime", dialogFlowOutputBean.getMeetingTime());
      context.put("meetingDate", dialogFlowOutputBean.getMeetingDate());
      context.put("meetingType", dialogFlowOutputBean.getMeetingType());
      //context.put("name",dialogFlowOutputBean.getName());
      context.put("fromname", AWSSESEmailSender.fromname);

      StringWriter writer = new StringWriter();
      template.merge(context, writer);
      return writer;
    } catch (Exception ex) {
      System.out.println("Error message: " + ex.getMessage());
    }
    return null;
  }

  public static StringWriter missingInfoTemplate(DialogFlowOutputBean dialogFlowOutputBean) {
    VelocityEngine velocity = new VelocityEngine();
    velocity.setProperty("resource.loader", "class");
    velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Template template = velocity.getTemplate("email-templates/missing_info_template.html");
    velocity.init();
    VelocityContext context = new VelocityContext();
    try {
      System.out.println("First Result" + dialogFlowOutputBean);
      context.put("Technologies", dialogFlowOutputBean.getTechnologies());
      context.put("meetingTime", dialogFlowOutputBean.getMeetingTime());
      context.put("meetingDate", dialogFlowOutputBean.getMeetingDate());
      context.put("meetingType", dialogFlowOutputBean.getMeetingType());
      context.put("name", dialogFlowOutputBean.getName());

      StringWriter writer = new StringWriter();
      template.merge(context, writer);
      return writer;
    } catch (Exception ex) {
      System.out.println("Error message: " + ex.getMessage());
    }
    return null;
  }

  public static StringWriter meetingScheduleTemplate(DialogFlowOutputBean dialogFlowOutputBean) {
    VelocityEngine velocity = new VelocityEngine();
    velocity.setProperty("resource.loader", "class");
    velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Template template = velocity.getTemplate("email-templates/meeting_Schedule_template.html");
    velocity.init();
    VelocityContext context = new VelocityContext();
    try {
      System.out.println("First Result" + dialogFlowOutputBean);
      context.put("Technologies", dialogFlowOutputBean.getTechnologies());
      context.put("meetingTime", dialogFlowOutputBean.getMeetingTime());
      context.put("meetingDate", dialogFlowOutputBean.getMeetingDate());
      context.put("meetingType", dialogFlowOutputBean.getMeetingType());
      context.put("name", dialogFlowOutputBean.getName());

      StringWriter writer = new StringWriter();
      template.merge(context, writer);
      return writer;
    } catch (Exception ex) {
      System.out.println("Error message: " + ex.getMessage());
    }
    return null;
  }

  public static StringWriter missingTimeDateTemplate(DialogFlowOutputBean dialogFlowOutputBean) {
    VelocityEngine velocity = new VelocityEngine();
    velocity.setProperty("resource.loader", "class");
    velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Template template = velocity.getTemplate("email-templates/missing_time_date_template.html");
    velocity.init();
    VelocityContext context = new VelocityContext();
    try {
      System.out.println("First Result" + dialogFlowOutputBean);
      context.put("Technologies", dialogFlowOutputBean.getTechnologies());
      context.put("meetingTime", dialogFlowOutputBean.getMeetingTime());
      context.put("meetingDate", dialogFlowOutputBean.getMeetingDate());
      context.put("meetingType", dialogFlowOutputBean.getMeetingType());
      context.put("name", dialogFlowOutputBean.getName());

      StringWriter writer = new StringWriter();
      template.merge(context, writer);
      return writer;
    } catch (Exception ex) {
      System.out.println("Error message: " + ex.getMessage());
    }
    return null;
  }

  public static StringWriter meetingConfirmationICSTemplate(DialogFlowOutputBean dialogFlowOutputBean) {
    VelocityEngine velocity = new VelocityEngine();
    velocity.setProperty("resource.loader", "class");
    velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Template template = velocity.getTemplate("email-templates/ICSTemplate.ics");
    velocity.init();
    VelocityContext context = new VelocityContext();
    try {
      System.out.println("First Result" + dialogFlowOutputBean);
      context.put("Technologies", dialogFlowOutputBean.getTechnologies());
      context.put("meetingTime", dialogFlowOutputBean.getMeetingTime());
      context.put("meetingDate", dialogFlowOutputBean.getMeetingDate());
      context.put("meetingType", dialogFlowOutputBean.getMeetingType());
      context.put("name", dialogFlowOutputBean.getName());

      StringWriter writer = new StringWriter();
      template.merge(context, writer);
      return writer;
    } catch (Exception ex) {
      System.out.println("Error message: " + ex.getMessage());
    }
    return null;
  }


  public static StringWriter registrationTemplate() {
    VelocityEngine velocity = new VelocityEngine();
    velocity.setProperty("resource.loader", "class");
    velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Template template = velocity.getTemplate("email-templates/registration_template.html");
    velocity.init();
    VelocityContext context = new VelocityContext();
    try {
      StringWriter writer = new StringWriter();
      template.merge(context, writer);
      return writer;
    } catch (Exception ex) {
      System.out.println("Error message: " + ex.getMessage());
    }
    return null;
  }


}

