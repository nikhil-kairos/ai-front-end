package com.powerup.email.template;
import java.io.StringWriter;

import com.powerup.email.AWSSESEmailSender;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

public class RandomIdSampleTemplate
{
  public static void main(String[] args) {
    VelocityContext context = new VelocityContext();
    VelocityEngine velocity = new VelocityEngine();
    velocity.setProperty("resource.loader", "class");
    velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Template template = velocity.getTemplate("email-templates/random_id.html");
    velocity.init();
    context.put("from", AWSSESEmailSender.from);
    context.put("fromname", AWSSESEmailSender.fromname);
    context.put("to", AWSSESEmailSender.to);
    context.put("bcc", AWSSESEmailSender.bcc[0]);
    context.put("title", "Apache Velocity");
    context.put("subject", AWSSESEmailSender.uniqueId);
    context.put("messageBody", AWSSESEmailSender.messageBody);
    StringWriter writer = new StringWriter();
    template.merge(context, writer);
    System.out.println(writer.toString());
  }
  }
