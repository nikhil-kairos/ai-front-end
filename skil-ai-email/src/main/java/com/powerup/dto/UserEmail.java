package com.powerup.dto;

import org.springframework.context.annotation.Bean;

public class UserEmail {
  public String email;
  public String name;
  public String id;
  public String idToken;
  public String image;
  public String provider;
  public String token;

  public String getEmail() {    return email;  }

  public void setEmail(String email) {    this.email = email;  }

  public String getName() {    return name;  }

  public void setName(String name) {    this.name = name;  }

  public String getId() {    return id;  }

  public void setId(String id) {    this.id = id;  }

  public String getIdToken() {    return idToken;  }

  public void setIdToken(String idToken) {    this.idToken = idToken;  }

  public String getImage() {    return image;  }

  public void setImage(String image) {    this.image = image;  }

  public String getProvider() {    return provider;  }

  public void setProvider(String provider) {    this.provider = provider;  }

  public String getToken() {    return token;  }

  public void setToken(String token) {    this.token = token;  }

  @Override
  public String toString() {
    return "UserEmail{" +
            "email='" + email + '\'' +
            ", name='" + name + '\'' +
            ", id='" + id + '\'' +
            ", idToken='" + idToken + '\'' +
            ", image='" + image + '\'' +
            ", provider='" + provider + '\'' +
            ", token='" + token + '\'' +
            '}';
  }
}
