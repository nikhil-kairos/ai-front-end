package com.powerup.dto;

import java.util.LinkedList;
public class DialogFlowOutputBean {

  private String intent;

  public LinkedList<String> getTechnologies() {
    return technologies;
  }

  public void setTechnologies(LinkedList<String> technologies) {
    this.technologies = technologies;
  }

  private LinkedList<String> technologies;

  public String getMeetingTime() {
    return meetingTime;
  }

  public void setMeetingTime(String meetingTime) {
    this.meetingTime = meetingTime;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMeetingType() {
    return meetingType;
  }

  public void setMeetingType(String meetingType) {
    this.meetingType = meetingType;
  }

  private String name;
  private String meetingType;

  private String meetingTime;
  private String meetingDate;
  private LinkedList<String> softSkills;
  private LinkedList<String> educations;
  private LinkedList<String> jobRoles;

  public String getIntent() {
    return intent;
  }

  public void setIntent(String intent) {
    this.intent = intent;
  }


  public String getMeetingDate() {
    return meetingDate;
  }

  public void setMeetingDate(String meetingDate) {
    this.meetingDate = meetingDate;
  }

  public LinkedList<String> getSoftSkills() {
    return softSkills;
  }

  public void setSoftSkills(LinkedList<String> softSkills) {
    this.softSkills = softSkills;
  }

  public LinkedList<String> getEducations() {
    return educations;
  }

  public void setEducations(LinkedList<String> educations) {
    this.educations = educations;
  }

  public LinkedList<String> getJobRoles() {
    return jobRoles;
  }

  public void setJobRoles(LinkedList<String> jobRoles) {
    this.jobRoles = jobRoles;
  }
}

