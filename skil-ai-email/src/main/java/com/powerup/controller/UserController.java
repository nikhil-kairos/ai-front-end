package com.powerup.controller;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.powerup.email.EmailSender;
import com.powerup.email.template.VelocityEmailTemplate;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.web.bind.annotation.*;
import com.powerup.model.UserDetails;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {
  @RequestMapping("/user")
  public UserDetails userDetails (String name){
    return userDetails("KAIROS");
  }

  @PostMapping("/user")
  public void storeUserDetails (@RequestBody UserDetails userDetails) throws Exception {

    String email1 = (String) userDetails.get("email");
    Date createdAt1 = new Date();
    Date updatedAt1 = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
    formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
    String createdAt = formatter.format(createdAt1);
    String updatedAt = formatter.format(updatedAt1);

    MongoClientURI uri = new MongoClientURI("mongodb://127.0.0.1:27017");
    MongoClient mongoClient = new MongoClient(uri);
    try {
      MongoDatabase database = mongoClient.getDatabase("powerup-ai-email");
      MongoCollection<Document> collection = database.getCollection("lucy-ai-emails");

      long email1Count = collection.countDocuments(Filters.and(Filters.eq("email",email1)));
      if (email1Count == 0) {
        userDetails.append("createdAt", createdAt);
        userDetails.append("updatedAt", updatedAt);
        collection.insertOne(userDetails);
        String messageBody1 = VelocityEmailTemplate.registrationTemplate().toString();
        EmailSender.messageBody = messageBody1;
        EmailSender.sendMail(email1, "testMail", EmailSender.messageBody);
        System.out.println("Email Sent Successfully");

      } else{
        Bson filter = new Document("email", email1);
        Bson newValue = new Document("updatedAt", new Date());
        Bson updateOperationDocument = new Document("$set", newValue);

        collection.updateOne(filter, updateOperationDocument);
      }

    } finally {
      //Close the client and release resources.
      mongoClient.close();
    }
  }
}

