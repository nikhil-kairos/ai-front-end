package com.powerup.controller;

import com.powerup.model.Greeting1;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

  @RequestMapping("/greeting")
  public Greeting1 greeting1(@RequestParam(value = "name", defaultValue = "World") String name) {
    return new Greeting1("Hello Ninjas Team.......");
  }

}